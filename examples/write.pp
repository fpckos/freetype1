(***************************************************************************
 *
 *  Write    FreeType simple text renderer.                              1.0
 *
 *
 *    This small program will load a TrueType font file and display
 *    a small line of text.
 *
 *  This source code has been compiled and run under both Virtual Pascal
 *  on OS/2 and Borland's BP7.
 *
 *  Copyright 1996-2000 David Turner, Robert Wilhelm and Werner Lemberg.
 *
 *  This file is part of the FreeType project, and may only be used
 *  modified and distributed under the terms of the FreeType project
 *  license, LICENSE.TXT. By continuing to use, modify or distribute
 *  this file you indicate that you have read the license and
 *  understand and accept it fully.
 *
 ***************************************************************************)

(***************************************************************************
 *
 *  This program will been updated by bw.
 *
 *  This source code has been compiled and run under Free Pascal
 *  on Linux.
 *
 *  Copyright 2008 bw
 *
 ***************************************************************************)

program Write;

{$mode objfpc}

uses
  SysUtils,
  Common,
  GMain, GEvents, GDriver,
  FreeType;


const
  Precis  = 64;
  Precis2 = Precis div 2;
  PrecisAux = 1024;
  Profile_Buff_Size = 32000;
  Max_Files = 1024;


var
  face     : TT_Face;
  instance : TT_Instance;
  glyph    : TT_Glyph;
  char_map : TT_CharMap;

  metrics  : TT_Glyph_Metrics;
  imetrics : TT_Instance_Metrics;

  props    : TT_Face_Properties;

  ymin, ymax, xmax, xmin, xsize : longint;
  res, old_res                  : int;

  numPoints, numContours : int;

  Bit      : TT_Raster_Map;

  Rotation : int;  (* Angle modulo 1024 *)

  num_glyphs : int;

  error      : TT_Error;
  gray_level : Boolean;

  display_outline : boolean;
  hint_glyph      : boolean;
  scan_type       : Byte;

  old_glyph : int;
  cur_glyph : int;

  glyph_code  : array [1 .. 128] of Word;
  num_codes   : 1..128;

  scale_shift : Int;

  grayLines : array[0..2048] of Byte;

(*******************************************************************
 *
 *  Function    : Set_Raster_Area
 *
 *****************************************************************)

 procedure Set_Raster_Area;
 begin
   Bit.rows  := vio_Height;
   Bit.width := vio_Width;
   Bit.flow  := TT_Flow_Up;

   if gray_level then
     Bit.cols := Bit.width
   else
     Bit.cols := (Bit.width+7) div 8;

   Bit.size := Bit.rows * Bit.cols;
 end;

(*******************************************************************
 *
 *  Function    : Clear_Data
 *
 *****************************************************************)

 procedure Clear_Data;
 begin
   if gray_level then
     fillchar( Bit.buffer^, Bit.size, gray_palette[0] )
   else
     fillchar( Bit.buffer^, Bit.size, 0 );
 end;

(*******************************************************************
 *
 *  Function    : Init_Engine
 *
 *****************************************************************)

 procedure Init_Engine;
 begin
   Set_Raster_Area;
   GetMem( Bit.buffer, Bit.size );
   Clear_Data;
 end;

(*******************************************************************
 *
 *  Function    : Reset_Scale
 *
 *****************************************************************)

 function Reset_Scale( res : Int ) : Boolean;
 begin
   error := TT_Set_Instance_CharSize( instance, res*64 );
   Reset_Scale := (error = TT_Err_Ok);
 end;


(*******************************************************************
 *
 *  Function    : Erreur
 *
 *****************************************************************)

procedure Erreur(S: String; Terminate: Boolean=True);
begin
  if Terminate then Restore_Screen;
  WriteLn('Error: ' + s + ', error code = ' + IntToStr(error));
  if Terminate then Halt(1);
end;


(*******************************************************************
 *
 *  Function    : Usage
 *
 *****************************************************************)

procedure Usage(Terminate: Boolean=False);
begin
  Writeln('Simple string text display - part of the FreeType project' );
  Writeln;
  Writeln('Usage : ',paramStr(0),' [ptSize] FontName[.TTF] [string]');
  if Terminate then Halt(1);
end;



(*******************************************************************
 *
 *  Function    :  CharToGlyphID
 *
 *  Description :  Convert an ASCII string to a string of glyph
 *                 indexes.
 *
 * IMPORTANT NOTE:
 *
 * There is no portable way to convert from any system's char. code
 * to Unicode.  This function simply takes a char. string as argument
 * and "interprets" each character as a Unicode char. index with no
 * further check.
 *
 * This mapping is only valid for the ASCII character set (i.e.,
 * codes 32 to 127); all other codes (like accentuated characters)
 * will produce more or less random results, depending on the system
 * being run.
 *
 *****************************************************************)

 procedure CharToGlyphID( source : string );
 var
   i, n,
   platform, encoding : Word;
   found : Boolean;

 begin
    (* First, look for a Unicode charmap *)

    i := 0;
    repeat
      TT_Get_CharMap_ID( face, i, platform, encoding );
      found := ( (platform = 3) and (encoding = 1) )  or
               ( (platform = 0) and (encoding = 0) );
      if found then
      begin
        TT_Get_CharMap( face, i, char_map );
      end
      else
        Inc(i);
    until found or (i>=props.num_charmaps);

    if not found then
      Erreur( 'Sorry, but this font doesn''t contain any Unicode mapping table' );

    n := 1;
    while (n <= 128) and (n <= Length(source)) do begin
{$IFNDEF PROVIDE_TOUNICODE}
      glyph_code[n] := TT_Char_Index( char_map, Ord(source[n]) );
{$ELSE}
    (* Note, if you have a function, say ToUnicode(), to convert from     *)
    (* char codes to Unicode, use the following line instead:             *)

      glyph_code[n] := TT_Char_Index( char_map, ToUnicode( source[n] ) );
{$ENDIF}
      Inc(n);
    end;

    num_codes := n-1;
  end;


(*******************************************************************
 *
 *  Function    :  LoadTrueTypeChar
 *
 *  Description :  Loads a single glyph into the xcoord, ycoord and
 *                 flag arrays, from the instance data.
 *
 *****************************************************************)

 function LoadTrueTypeChar( index : integer;
                            hint  : boolean ) : TT_Error;
 var
   j, load_flag : int;

   res : TT_Error;

 begin
   if hint then load_flag := TT_Load_Scale_Glyph or TT_Load_Hint_Glyph
           else load_flag := TT_Load_Scale_Glyph;

   res := TT_Load_Glyph( instance,
                         glyph,
                         index,
                         load_flag );

   LoadTrueTypeChar := res;
 end;


(*******************************************************************
 *
 *  Function    : Render_All
 *
 *****************************************************************)

var
  Error_String : String;
  ine : Int;

function Render_All : boolean;
var
  i, j : integer;

  x, y, z : longint;

  minx, miny, maxx, maxy : TT_F26Dot6;

  fail : Int;
begin

  Render_All := True;

  (* On the first pass, we compute the compound bounding box. *)

  x := 0;
  y := 0;

  minx := 0;
  miny := 0;
  maxx := 0;
  maxy := 0;

  for i := 1 to num_codes do
  begin
    if LoadTrueTypeChar( glyph_code[i], hint_glyph ) = TT_Err_OK then
    begin
      TT_Get_Glyph_Metrics( glyph, metrics );

      z := x + metrics.bbox.xMin;
      if minx > z then
        minx := z;

      z := x + metrics.bbox.xMax;
      if maxx < z then
        maxx := z;

      z := y + metrics.bbox.yMin;
      if miny > z then
        miny := z;

      z := y + metrics.bbox.yMax;
      if maxy < z then
        maxy := z;

      Inc(x, metrics.advance and -64);
    end
    else
      Inc(Fail);
  end;

  (* We now center the bbox inside the target bitmap *)

  minx := ( minx and -64 ) shr 6;
  miny := ( miny and -64 ) shr 6;

  maxx := ( (maxx+63) and -64 ) shr 6 - minx;
  maxy := ( (maxy+63) and -64 ) shr 6 - miny;

  if maxx > Bit.width then maxx := Bit.width;
  if maxy > Bit.rows  then maxy := Bit.rows;

  minx := (Bit.width - maxx) div 2;
  miny := (Bit.rows  - maxy) div 2;

  Inc(maxx, minx);
  Inc(maxy, miny);


  (* On the second pass, we render each glyph to its centered position. *)
  (* This is slow, because we reload each glyph to render it!           *)

  x := minx;
  y := miny;

  for i := 1 to num_codes do
  begin
    if LoadTrueTypeChar( glyph_code[i], hint_glyph ) = TT_Err_OK then
    begin
      TT_Get_Glyph_Metrics( glyph, metrics );

      if gray_level then
        TT_Get_Glyph_Pixmap( glyph, Bit, x*64, y*64 )
      else
        TT_Get_Glyph_Bitmap( glyph, Bit, x*64, y*64 );

      inc(x, metrics.advance div 64);
    end;
  end;

  Render_All := False;
end;


(*******************************************************************
 *
 *  Main program
 *
 *****************************************************************)

var
  BasePath : String;
  Filename : String;

  Fail     : Int;
  glyphStr : String[4];
  ev       : Event;

  Code     : Int;

  init_memory, end_memory : LongInt;

  num_args   : Integer;
  point_size : Integer;
  cur_file   : Integer;
  first_arg  : Int;
  ended      : Boolean;
  valid      : Boolean;
  errmsg     : String;

  I: integer;
  C: Char;
  F: TSearchRec;

label
  Lopo;

begin
  TT_Init_FreeType;

  num_args   := ParamCount;
  point_size := 24;
  gray_level := False;
  BasePath   := ExtractFilePath(ParamStr(0));

  {if num_args = 0 then Usage;}
  if num_args = 0 then
  begin
    if FindFirst(BasePath + '*.ttf', faAnyFile, F) = 0 then
    begin
      FileName := BasePath + F.Name;
      Usage(False);
    end else
      Usage;
  end else

  begin
    first_arg := 1;

    if ParamStr(first_arg) = '-g' then
    begin
      Inc(first_arg);
      gray_level := True;
    end;

    if first_arg > num_args + 1 then
      Usage;

    Val(ParamStr(first_arg), point_size, Code);
    if Code = 0 then
      Inc(first_arg);

    if first_arg > num_args + 1 then
      Usage;

    FileName := ParamStr(first_arg);
    if Pos('.', FileName) = 0 then FileName := FileName + '.ttf';
  end;

  if not gray_level then
  if not Set_Graph_Screen(Graphics_Mode_Mono) then
  begin
    Erreur('Could not set mono graphics mode', False);
    gray_level := True;
  end;

  if gray_level then
  if not Set_Graph_Screen(Graphics_Mode_Gray) then
    Erreur('Could not set grayscale graphics mode');

  Init_Engine;

  valid := True;

  error := TT_Open_Face( filename, face );
  if error <> TT_Err_Ok then
  begin
    str( error, errmsg );
    errmsg := 'Could not open ' + filename + ', error code = '+errmsg;
    valid  := false;
    goto Lopo;
  end;

  TT_Get_Face_Properties( face, props );

  num_glyphs := props.num_Glyphs;

  i := length(FileName);
  while (i > 0) and (FileName[i] <> '\') do dec(i);

  FileName := Copy( FileName, i+1, length(FileName) );

  error := TT_New_Glyph( face, glyph );
  if error <> TT_Err_Ok then
    Erreur('Could not create glyph container');

  error := TT_New_Instance( face, instance );
  if error <> TT_Err_Ok then
  begin
    str( error, errmsg );
    errmsg := 'Could not create instance, error code = '+errmsg;
    valid  := false;
    goto Lopo;
  end;

  TT_Set_Instance_Resolutions( instance, 96, 96 );

  Rotation  := 0;
  Fail      := 0;
  res       := point_size;
  scan_type := 2;

  Reset_Scale( res );

  if first_arg >= num_args then
    CharToGlyphID( 'The quick brown fox jumps over the lazy dog' )
  else
    CharToGlyphID( ParamStr( first_arg + 1 ) );

Lopo:

  display_outline := true;
  hint_glyph      := true;

  old_glyph := -1;
  old_res   := res;
  cur_glyph := 0;

  ended := false;

  Repeat

    if valid then
    begin
      Clear_Data;

      if Render_All then
        inc( Fail )
      else
        Display_Bitmap_On_Screen( Bit.Buffer^, Bit.rows, Bit.cols  );

      Print_XY( 0, 0, FileName );

      TT_Get_Instance_Metrics( instance, imetrics );

      Print_Str('  pt size = ');
      Str( imetrics.pointSize div 64:3, glyphStr );
      Print_Str( glyphStr );

      Print_Str('  ppem = ');
      Str( imetrics.y_ppem:3, glyphStr );
      Print_Str( glyphStr );

      Print_XY( 0, 1, 'Hinting  (''z'')  : ' );
      if hint_glyph then Print_Str('on ')
                    else Print_Str('off');

      Print_XY( 0, 2, 'scan type(''e'')  : ' );
      case scan_type of
        0 : Print_Str('none   ');
        1 : Print_Str('level 1');
        2 : Print_Str('level 2');
        4 : Print_Str('level 4');
        5 : Print_Str('level 5');
      end;
    end
    else
    begin
      Clear_Data;
      Display_Bitmap_On_Screen( Bit.buffer^, Bit.rows, Bit.cols );
      Print_XY( 0, 0, errmsg );
    end;

    Get_Event(ev);

    case ev.what of
      event_Quit : ended := True;

      event_Keyboard :
        case char(ev.info) of
          'h',
          'z' : hint_glyph := not hint_glyph;

          'e' : begin
                  inc( scan_type );
                  if scan_type  = 3 then scan_type := 4;
                  if scan_type >= 6 then scan_type := 0;
                end;
         end;

      event_Scale_Glyph :
        begin
          inc( res, ev.info );
          if res < 1 then    res := 1;
          if res > 1400 then res := 1400;
        end;

      event_Change_Glyph :
        begin
        end;
    end;

    if res <> old_res then
      begin
        if not Reset_Scale(res) then
          Erreur( 'Could not resize font' );
        old_res := res;
      end;

  Until ended;

  TT_Done_Glyph( glyph );
  TT_Close_Face( face );

  Restore_Screen;

  Writeln;
  if Fail = 0 then
    Writeln('No fails.') else
    Writeln('Fails: ', Fail);

  TT_Done_FreeType;
end.
