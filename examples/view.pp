(***************************************************************************
 *
 *  View    FreeType Glyph Viewer.                                       1.0
 *
 *
 *    This small program will load a TrueType font file and allow
 *    you to view/scale/rotate its glyphs. Glyphs are in the order
 *    found within the 'glyf' table.
 *
 *  NOTE : This version displays a magnified view of the glyph
 *         along with the pixel grid.
 *
 *  This source code has been compiled and run under both Virtual Pascal
 *  on OS/2 and Borland's BP7.
 *
 *  Copyright 1996-1999 David Turner, Robert Wilhelm and Werner Lemberg.
 *
 *  This file is part of the FreeType project, and may only be used
 *  modified and distributed under the terms of the FreeType project
 *  license, LICENSE.TXT. By continuing to use, modify or distribute
 *  this file you indicate that you have read the license and
 *  understand and accept it fully.
 *
 ***************************************************************************)

(***************************************************************************
 *
 *  This program will been updated by bw.
 *
 *  This source code has been compiled and run under Free Pascal
 *  on KolibriOS.
 *
 *  Copyright 2008 bw
 *
 ***************************************************************************)

program View;

{$mode objfpc}

uses
  SysUtils,
  Common,
  GMain, GEvents, GDriver,
  FreeType;


const
  Precis  = 64;
  Precis2 = Precis div 2;
  PrecisAux = 1024;
  Profile_Buff_Size = 32000;
  Max_Files = 1024;


var
  face     : TT_Face;
  instance : TT_Instance;
  glyph    : TT_Glyph;

  metrics  : TT_Glyph_Metrics;
  imetrics : TT_Instance_Metrics;

  props    : TT_Face_Properties;

  ymin, ymax, xmax, xmin, xsize : longint;
  res, old_res                  : int;

  numPoints, numContours : int;

  Bit : TT_Raster_Map;

  Rotation : int;  (* Angle modulo 1024 *)

  num_glyphs : int;

  error      : TT_Error;
  gray_level : Boolean;

  display_outline : boolean;
  hint_glyph      : boolean;
  scan_type       : Byte;

  old_glyph : int;
  cur_glyph : int;

  scale_shift : Int;

  grayLines : array[0..2048] of Byte;

  Fail: Int;

(*******************************************************************
 *
 *  Function    : Set_Raster_Area
 *
 *****************************************************************)

 procedure Set_Raster_Area;
 begin
   Bit.rows  := vio_Height;
   Bit.width := vio_Width;
   Bit.flow  := TT_Flow_Up;

   if gray_level then
     Bit.cols := Bit.width else
     Bit.cols := (Bit.width + 7) div 8;

   Bit.size := Bit.rows * Bit.cols;
 end;

(*******************************************************************
 *
 *  Function    : Clear_Data
 *
 *****************************************************************)

 procedure Clear_Data;
 begin
   if gray_level then
     FillChar(Bit.buffer^, Bit.size, gray_palette[0]) else
     FillChar(Bit.buffer^, Bit.size, 0);
 end;

(*******************************************************************
 *
 *  Function    : Init_Engine
 *
 *****************************************************************)

 procedure Init_Engine;
 begin
   Set_Raster_Area;
   GetMem(Bit.buffer, Bit.size);
   Clear_Data;
 end;

(*******************************************************************
 *
 *  Function    : Reset_Scale
 *
 *****************************************************************)

 function Reset_Scale(res: Int) : Boolean;
 begin
   error := TT_Set_Instance_CharSize(instance, res * 64);
   Result := (error = TT_Err_Ok);
 end;


(*******************************************************************
 *
 *  Function    :  LoadTrueTypeChar
 *
 *  Description :  Loads a single glyph into the xcoord, ycoord and
 *                 flag arrays, from the instance data.
 *
 *****************************************************************)

function LoadTrueTypeChar(index: Integer; hint: Boolean): TT_Error;
var
  load_flag: Int;
begin
  if hint then
    load_flag := TT_Load_Scale_Glyph or TT_Load_Hint_Glyph else
    load_flag := TT_Load_Scale_Glyph;
  Result := TT_Load_Glyph(instance, glyph, index, load_flag);
end;


(*******************************************************************
 *
 *  Function    : Render_All
 *
 *****************************************************************)

var
  Error_String: String;
  ine: Int;

function Render_All(glyph_index: Integer): Boolean;
var
  i, j: Integer;
  x, y: Longint;
  start_x, start_y: Longint;
  step_x, step_y  : Longint;
  Error: TT_Error;

begin
  TT_Get_Instance_Metrics(instance, imetrics);

  start_x := 4;
  start_y := vio_Height - 30 - imetrics.y_ppem;

  step_x := imetrics.x_ppem + 4;
  step_y := imetrics.y_ppem + 10;

  x := start_x;
  y := start_y;

  ine := glyph_index;
  while ine < num_glyphs do
  begin
    Error := LoadTrueTypeChar(ine, hint_glyph);
    if Error = TT_Err_Ok then
    begin
      TT_Get_Glyph_Metrics(glyph, metrics);

      if gray_level then
        TT_Get_Glyph_Pixmap(glyph, Bit, x * 64, y * 64) else
        TT_Get_Glyph_Bitmap(glyph, Bit, x * 64, y * 64);

      if metrics.advance = 0 then
        Inc(x, step_x) else
        Inc(x, (metrics.advance div 64) + 1);

      if x > vio_Width - 40 then
      begin
        x := start_x;
        Dec(y, step_y);
        if y < 10 then
        begin
          Result := False;
          Exit;
        end;
      end;
    end else
    begin
//       WriteLn(Format('Error #%d on loading glyph #%d', [Error, ine]));
      Inc(Fail);
    end;

    Inc(ine);
  end;

  Result := False;
end;



(*******************************************************************
 *
 *  Function    : Erreur
 *
 *****************************************************************)

procedure Erreur(S: String; Terminate: Boolean = True);
begin
  if Terminate then Restore_Screen;
  WriteLn('Error: ' + s + ', error code = ' + IntToStr(error));
  if Terminate then Halt(1);
end;


(*******************************************************************
 *
 *  Function    : Usage
 *
 *****************************************************************)

procedure Usage;
begin
  WriteLn('Simple TrueType Glyphs viewer - part of the FreeType project' );
  WriteLn;
  WriteLn('Usage: ' + ParamStr(0) + ' [ptSize] [FontName[.TTF]]');
end;



(*******************************************************************
 *
 *  Main program
 *
 *****************************************************************)

var
  BasePath: String;
  FileName: String;
  glyphStr: String[4];
  ev      : Event;

  Code    : Int;

  init_memory, end_memory : LongInt;

  num_args  : Integer;
  point_size: Integer;
  cur_file  : Integer;
  first_arg : Integer;
  ended     : Boolean;
  next_file : Boolean;
  valid     : Boolean;
  errmsg    : String;

  I: Integer;
  F: TSearchRec;

label
  Lopo;

begin
  TT_Init_FreeType;

  BasePath := ExtractFilePath(ParamStr(0));
  num_args := ParamCount;

  point_size := 24;
  gray_level := False;
  num_arguments := 0;

  if num_args = 0 then
  begin
    Usage;

    if FindFirst(BasePath + '*.ttf', faAnyFile, F) = 0 then
    repeat
      FileName := BasePath + F.Name;
      GetMem(arguments[num_arguments], Length(FileName) + 1);
      arguments[num_arguments]^ := FileName;
      Inc(num_arguments);
    until FindNext(F) <> 0;
  end else

  begin
    first_arg := 1;

    if ParamStr(first_arg) = '-g' then
    begin
      Inc(first_arg);
      gray_level := True;
    end;

    Val(ParamStr(first_arg), I, Code);
    if Code = 0 then
    begin
      point_size := I;
      Inc(first_arg);
    end;

    Expand_Wildcards(first_arg, '.ttf');

    // @todo: arguments[...] = ...
  end;

  if num_arguments = 0 then
  begin
    Writeln('Could not find file(s)');
    Halt(3);
  end;

  cur_file := 0;

  if not gray_level then
  if not Set_Graph_Screen(Graphics_Mode_Mono) then
  begin
    Erreur('Could not set mono graphics mode', False);
    gray_level := True;
  end;

  if gray_level then
  if not Set_Graph_Screen(Graphics_Mode_Gray) then
    Erreur('Could not set grayscale graphics mode');

  Init_Engine;

  ended := false;

  repeat
      valid := True;

      FileName := ExpandFileName(arguments[cur_file]^);
      if Pos('.', FileName) = 0 then FileName := FileName + '.ttf';

      error := TT_Open_Face(FileName, face);
      if error <> TT_Err_Ok then
      begin
        errmsg := 'Could not open "' + FileName + '", error code = ' + IntToStr(error);
        Valid  := False;
        goto Lopo;
      end;

      TT_Get_Face_Properties(face, props);

      num_glyphs := props.num_Glyphs;

      error := TT_New_Glyph(face, glyph);
      if error <> TT_Err_Ok then
        Erreur('Could not create glyph container');

      error := TT_New_Instance(face, instance);
      if error <> TT_Err_Ok then
        Erreur('Could not create instance, error code = ' + IntToStr(error));

      TT_Set_Instance_Resolutions(instance, 96, 96);

      Rotation  := 0;
      Fail      := 0;
      res       := point_size;
      scan_type := 2;

      if not Reset_Scale(res) then
        Erreur('Could not reset scale, error #' + IntToStr(Error));

  Lopo:
      display_outline := True;
      hint_glyph      := True;

      old_glyph := -1;
      old_res   := res;
      cur_glyph := 0;

      next_file := False;

      repeat
        if Valid then
        begin
          if Render_All(cur_glyph) then
            Inc(Fail) else
            Display_Bitmap_On_Screen(Bit.Buffer^, Bit.rows, Bit.cols);

          Clear_Data;

          Print_XY(0, 0, ExtractFileName(FileName));

          TT_Get_Instance_Metrics(instance, imetrics);

          if gray_level then
            Print_Str(' | pt=') else
            Print_Str(' | pt size=');
          Print_Str(IntToStr(imetrics.pointSize div 64));

          if gray_level then
            Print_Str(' glyph=') else
            Print_Str(' glyph>=');
          Print_Str(IntToStr(cur_glyph));

          Print_XY(0, 1, ' Hinting(''z'')=' );
          if hint_glyph then
            Print_Str('on') else
            Print_Str('off');

          Print_XY(0, 2,' ScanType(''e'')=' );
          case scan_type of
            0: Print_Str('none');
            1: Print_Str('level 1');
            2: Print_Str('level 2');
            4: Print_Str('level 4');
            5: Print_Str('level 5');
          end;

          Print_XY(0, 3,' | Keys: 9,0,i,o,k,l; +,-,u,j; n,p');
        end else

        begin
          Clear_Data;
          Display_Bitmap_On_Screen(Bit.buffer^, Bit.rows, Bit.cols);
          Print_XY(0, 0, errmsg);
        end;

        Get_Event(ev);

        case ev.what of
          event_Quit:
            Ended := True;

          event_Keyboard:
            case Char(ev.info) of
              'n': begin
                next_file := true;
                if cur_file + 1 < num_arguments then
                  inc( cur_file );
              end;

              'p': begin
                next_file := true;
                if cur_file > 0 then
                  dec( cur_file );
              end;

              'h', 'z':
                hint_glyph := not hint_glyph;

              'e': begin
                Inc(scan_type);
                if scan_type  = 3 then scan_type := 4;
                if scan_type >= 6 then scan_type := 0;
              end;
            end;

          event_Scale_Glyph: begin
            Inc(res, ev.info);
            if res < 1 then    res := 1;
            if res > 1400 then res := 1400;
          end;

          event_Change_Glyph: begin
            inc( cur_glyph, ev.info );
            if cur_glyph < 0 then cur_glyph := 0;
            if cur_glyph >= num_glyphs then
              cur_glyph := num_glyphs-1;
          end;
        end;

        if res <> old_res then
        begin
          if not Reset_Scale(res) then
            Erreur('Could not resize font');
          old_res := res;
        end;

      until Ended or next_file;

      TT_Done_Glyph(glyph);
      TT_Close_Face(face);
  until ended;

  Restore_Screen;

  Writeln;
  if Fail = 0 then
    WriteLn('No fails.') else
    WriteLn('Fails: ', Fail);

  TT_Done_FreeType;
end.
