(***************************************************************************
 *
 *  Common    common utilities                                           1.0
 *
 *
 *  Copyright 1997-1999 David Turner, Robert Wilhelm and Werner Lemberg.
 *
 *  This file is part of the FreeType project, and may only be used
 *  modified and distributed under the terms of the FreeType project
 *  license, LICENSE.TXT. By continuing to use, modify or distribute
 *  this file you indicate that you have read the license and
 *  understand and accept it fully.
 *
 ***************************************************************************)

Unit Common;

interface

(* Define the USE32 constant on 32-bit systems. Virtual Pascal always    *)
(* define it by default. Now set for Delphi and FreePascal.              *)
    {$IFDEF WIN32} {$DEFINE USE32} {$ENDIF}
    {$IFDEF FPC}   {$DEFINE USE32} {$ENDIF}

{$IFDEF USE32}
type Cardinal = 0 .. MaxLongInt;
{$ELSE}
type Cardinal = Word;
{$ENDIF}

const
  Max_Arguments = 1024;

var
  num_arguments  : integer;
  (* the number of arguments contained in the 'arguments' array *)

  arguments      : array[0..Max_Arguments-1] of ^string;
  (* This array will hold all arguments after wildcard expansion *)
  (* note that it will not contain the original arguments that   *)
  (* were before 'first_argument' of Expand_Wildcards            *)

  procedure Expand_WildCards( first_argument    : integer;
                              default_extension : string );
  (* expand all wildcards into filenames *)

implementation

  (* Find the unit that declares the FindFirst/FindNext functions.  *)
  (* Not a trivial task...                                          *)
uses SysUtils;
{$DEFINE WINDOWS_TYPES}
{$DEFINE NO_DOSERROR}

{$IFDEF WINDOWS_TYPES}
type SearchRec = TSearchRec;
const
  ReadOnly = faReadOnly;
  Hidden   = faHidden;
  SysFile  = faSysFile;
  Directory= faDirectory;
  Archive  = faArchive;
  AnyFile  = faAnyFile;
{$ENDIF}

  procedure Split( Original : String;
                   var Base : String;
                   var Name : String );
  var
    n : integer;
  begin
    n := length(Original);

    while ( n > 0 ) do
      if ( Original[n] = '\' ) or
         ( Original[n] = '/' ) then
        begin
          Base := Copy( Original, 1, n-1 ) + '\';
          Name := Copy( Original, n+1, length(Original) );
          exit;
        end
      else
        dec(n);

    if (Upcase(Original[1]) in ['A'..'Z']) and (Original[2] = ':') then begin
      Base := Copy( Original, 1, 2 );
      Name := Copy( Original, 3, Length(Original) );
    end
    else begin
      Base := '';
      Name := Original;
    end;
  end;


  procedure Expand_WildCards( first_argument    : System.Integer;
                              default_extension : string );
  var
    i          : Integer;
    base, name : string;
{$IFDEF USE_CSTRING}
    ZTbase     : array [0 .. SizeOf(base)] of Char;
{$ENDIF}
{$IFDEF NO_DOSERROR}
    DosError   : Integer;
{$ENDIF}
    SRec       : SearchRec;
  begin
    num_arguments := 0;
    i             := first_argument;

    while ( i <= ParamCount ) do
    begin
      Split( ParamStr(i), base, name );

{$IFDEF USE_CSTRING}
      StrPCopy(ZTbase, base);
      StrPCopy(StrEnd(ZTbase),name);
  {$IFDEF NO_DOSERROR}
    DosError :=
  {$ENDIF}
      FindFirst( ZTbase, faArchive+faReadOnly+faHidden, SRec );
      if DosError <> 0 then begin
        StrPCopy(StrEnd(ZTbase), default_extension);
        FindFirst( ZTbase, faAnyFile, SRec );
      end;
{$ELSE}
  {$IFDEF NO_DOSERROR}
    DosError :=
  {$ENDIF}
      FindFirst( base+name, Archive+ReadOnly+Hidden, SRec );
      if DosError <> 0 then
        FindFirst( base+name+default_extension, AnyFile, SRec );
{$ENDIF}

      while (DosError = 0) and (num_arguments < Max_Arguments) do
      begin
{ XXX
        with SRec do
          while Name[Length(Name)] = #0 do Delete(Name, Length(Name), 1);
          (* strip trailing NULs *)
  XXX }
        GetMem( arguments[num_arguments], length(base)+length(SRec.Name)+1 );
        arguments[num_arguments]^ := base + SRec.Name;
        inc( num_arguments );
  {$IFDEF NO_DOSERROR}
      DosError :=
  {$ENDIF}
        FindNext( SRec );
      end;

      {$IFDEF USE32}
      FindClose( SRec );
      {$ENDIF}
      inc( i );
    end;
  end;

end.
