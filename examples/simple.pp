{$codepage utf8}

program Simple;

{$mode objfpc}


uses
  SysUtils, Classes,
  FPImage, FPCanvas, FPImgCanv, FPWriteBMP,
  FreeType;



function GetFontFamily(Face: TT_Face): String;
var
  Props: TT_Face_Properties;
  I, L: TT_UShort;
  P: Pointer;
  C: array[0..64] of Char;
  Plat, Enc, Lang, NameId: TT_UShort;
begin
  TT_Get_Face_Properties(Face, Props);
  for I := 0 to Props.num_Names - 1 do
  begin
    TT_Get_Name_ID(Face, I, Plat, Enc, Lang, NameId);
    {WriteLn('  [', I, '] - <', Plat, ', ', Enc, ', ', Lang, ', ', NameId, '>');}

    { TTCH02.DOC, "name - Naming Table", 47

      Platform: 0 - Apple Unicode
                1 - Macintosh
                2 - ISO
                3 - Microsoft

      Platform: 3
        Encoding: 0 - Undefined character set or indexing scheme
                  1 - UGL character set  with Unicode indexing scheme (see chapter, "Character Sets.")

      NameId: 0 - Copyright notice
              1 - Font Family name
              2 - Font Subfamily name
              3 - Unique font identifier
              4 - Full font name
              5 - Version string. In n.nn format
              6 - Postscript name for the font
              7 - Trademark
    }

    if (Plat = 1) and (Enc = 0) and (Lang = 0) and (NameId = 1) then
    begin
      TT_Get_Name_String(Face, I, P, L);
      if L >= SizeOf(C) then L := SizeOf(C) - 1;
      StrLCopy(@C, P, L);
      Result := StrPas(@C);
      Exit;
    end;
  end;

  Result := 'UNKNOWN';
end;

function IsItalic(Face: TT_Face): Boolean;
var
  Props: TT_Face_Properties;
begin
  TT_Get_Face_Properties(Face, Props);
  { TT_OS2.fsSelection:
      0	bit 1 - ITALIC     Font contains Italic characters, otherwise they are upright.
      1       - UNDERSCOR  Characters are underscored.
      2       - NEGATIVE   Characters have their foreground and background reversed.
      3       - OUTLINED   Outline (hollow) characters, otherwise they are solid.
      4       - STRIKEOUT  Characters are overstruck.
      5	bit 0 - BOLD       Characters are emboldened.
      6       - REGULAR    Characters are in the standard weight/style for the font.
  }
  Result := Props.os2^.fsSelection and 1 <> 0;
end;

function IsBold(Face: TT_Face): Boolean;
var
  Props: TT_Face_Properties;
begin
  TT_Get_Face_Properties(Face, Props);
  Result := Props.os2^.fsSelection and $20 <> 0;
end;


function GetCharMap(Face: TT_Face): TT_CharMap;
var
  Props: TT_Face_Properties;
  I: TT_UShort;
  Plat, Enc: TT_UShort;
begin
  TT_Get_Face_Properties(Face, Props);
  for I := 0 to Props.num_CharMaps - 1 do
  begin
    TT_Get_CharMap_ID(Face, I, Plat, Enc);
    if (Plat = 3) and (Enc = 1) then
    { Windows, Unicode }
    begin
      TT_Get_CharMap(Face, I, Result);
      Exit;
    end;
  end;
  Result.z := nil;  {XXX}
end;


procedure TestFont(Canvas: TFPCustomCanvas; FileName: String; Text: WideString; x, y: Longint; CharSize: Byte = 12);
var
  Face    : TT_Face;
  instance: TT_Instance;
  glyph   : TT_Glyph;
  metrics : TT_Glyph_Metrics;
  imetrics: TT_Instance_Metrics;
  Bit     : TT_Raster_Map;
  error   : TT_Error;
  CharMap  : TT_CharMap;
  CharIndex: TT_UShort;
  p: PByte;
  w: Word;

begin
  FileName := IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0))) + FileName;
  WriteLn('..load "', FileName, '"');

  error := TT_Open_Face(FileName, Face); {TODO: TT_Close_Face(face)}
  if error <> TT_Err_Ok then
  begin
    WriteLn('Could not open "', FileName, '", error code = ', error);
    Exit;
  end;

  Bit.width  := Canvas.Width;
  Bit.rows   := Canvas.Height;
  Bit.flow   := TT_Flow_Up;
  Bit.cols   := Bit.width;
  Bit.size   := Bit.rows * Bit.cols;
  Bit.buffer := GetMem(Bit.size);
  FillChar(Bit.buffer^, Bit.size, 0);

  try
    WriteLn('Font Family: ', GetFontFamily(Face));
    if IsItalic(Face) then WriteLn('Font is italic');
    if IsBold(Face)   then WriteLn('Font is bold');

    CharMap := GetCharMap(Face);

    error := TT_New_Glyph(Face, glyph); {TODO: TT_Done_Glyph(glyph)}
    if error <> TT_Err_Ok then
    begin
      WriteLn('Could not create glyph container');
      Exit;
    end;

    error := TT_New_Instance(Face, instance); {TODO: TT_Done_Instance(instance)}
    if error <> TT_Err_Ok then
    begin
      WriteLn('Could not create instance, error code = ', error);
      Exit;
    end;

    TT_Set_Instance_Resolutions(instance, 96, 96);
    TT_Set_Instance_CharSize(instance, CharSize * 64); {Byte -> TT_F26Dot6}
    TT_Get_Instance_Metrics(instance, imetrics);

    for w := 1 to Length(Text) do
    begin
      CharIndex := TT_Char_Index(CharMap, TT_ULong(Text[w]));
      error := TT_Load_Glyph(instance, glyph, CharIndex, TT_Load_Scale_Glyph or TT_Load_Hint_Glyph);
      if error <> TT_Err_Ok then
      begin
        WriteLn('Could not load glyph, error code = ', error);
        Exit;
      end;
      TT_Get_Glyph_Pixmap(glyph, Bit, x * 64, y * 64); {Longint -> TT_F26Dot6}
      {TT_Get_Glyph_Bitmap(glyph, Bit, x * 64, y * 64);}
      TT_Get_Glyph_Metrics(glyph, metrics);

      if metrics.advance = 0 then
        Inc(x, imetrics.x_ppem + 4) else
        Inc(x, (metrics.advance div 64) + 1);
    end;

    p := Bit.buffer;
    for y := 0 to Bit.rows - 1 do
    for x := 0 to Bit.width - 1 do
    begin
      if p^ > 31 then
      begin
        WriteLn('..{', p^, '} > 31');
        w := 65535;
      end else
        w := Round(p^ * 2114.02);
      Canvas.Colors[x, Bit.rows - y - 1] := FPColor(w, w, w);
      Inc(p);
    end;

  finally
    FreeMem(Bit.buffer);
    TT_Close_Face(face);
  end;
end;


procedure Paint(Canvas: TFPCustomCanvas);
begin
  TestFont(Canvas, 'times.ttf', 'Это просто текст.', 15, 10, 24);
  with Canvas do
  begin
    Pen.Style   := psSolid;
    Pen.FPColor := colGray;
    Brush.Style := bsClear;
    Rectangle(0, 0, Width - 1, Height - 1);
  end;
end;


var
  Image : TFPCustomImage;
  Canvas: TFPCustomCanvas;
  FileName: String;

begin
  TT_Init_FreeType;

  Randomize;
  Image := TFPMemoryImage.Create(700, 40);

  Canvas := TFPImageCanvas.Create(Image);
  Paint(Canvas);
  Canvas.Free;

  FileName := IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0))) + 'test.bmp';
  WriteLn('..save "', FileName, '"');
  Image.SaveToFile(FileName);

  TT_Done_FreeType;
end.
