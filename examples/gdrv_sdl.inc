
uses
  SDL, SDL_Video, SDL_Events, SDL_Keyboard;


const
  WIN_WIDTH   = 640;
  WIN_HEIGHT  = 450;


var
  Screen: PSDL_Surface = nil;


function Driver_Set_Graphics(Mode: Int): Boolean;
var
  I, J: Integer;
  Colors: array[0..31] of SDL_Color;

begin
  case Mode of
    Graphics_Mode_Gray: begin
      Vio_ScanLineWidth := WIN_WIDTH;
      Vio_Width         := WIN_WIDTH;
      Vio_Height        := WIN_HEIGHT;

      SDL_Init(SDL_INIT_VIDEO);
      Screen := SDL_SetVideoMode(WIN_WIDTH, WIN_HEIGHT, 8, SDL_SWSURFACE or SDL_HWPALETTE);

      if Assigned(Screen) then
      begin
        for I := 0 to 31 do
        begin
          gray_palette[I] := I;
          J := Round((31 - I) / 31 * 255);
          Colors[I].R := J;
          Colors[I].G := J;
          Colors[I].B := J;
          Colors[I].Unused := J;
        end;

        SDL_SetColors(Screen, PSDL_ColorArray(@Colors)^, 0, 32);

        Result := True;
      end else
      begin
        SDL_Quit;
        Result := False;
      end;
    end;

    else
      Result := False;
  end;
end;


function Driver_Restore_Mode: Boolean;
begin
  if Assigned(Screen) then
  begin
    SDL_FreeSurface(Screen);
    SDL_Quit;
    Screen := nil;
  end;
  Result := True;
end;


procedure Driver_Display_Bitmap(var Buffer; Lines, Columns: Int);
var
  I: Integer;
  PSrc, PDst: Pointer;
begin
  PSrc := @Buffer;
  PDst := Screen^.Pixels + (Lines - 1) * WIN_WIDTH;

  for I := 0 to Lines - 1 do
  begin
    Move(PSrc^, PDst^, Columns);
    Inc(PSrc, WIN_WIDTH);
    Dec(PDst, WIN_WIDTH);
  end;

  SDL_UpdateRect(Screen, 0, 0, Columns, Lines);
end;


function ReadKey: Char;
var
  Event: SDL_Event;
  Sym  : SDLKey;
  Caption: AnsiString;
begin
  Caption := Header + #0;
  SDL_WM_SetCaption(PChar(@Caption[1]), nil);

  while True do
  begin
    SDL_WaitEvent(@Event);
    case Event.EventType of
      SDL_EVENTQUIT: begin
        Result := #27;
        Break;
      end;

      SDL_KEYDOWN: begin
        Sym := SDL_KeyboardEvent(Event).keysym.sym;
        if (0 <= Sym) and (Sym <= 255) then
        begin
          Result := Char(Sym);
          Break;
        end;
      end;
    end;
  end;
end;
