(***************************************************************************
 *
 *  GDriver     Graphics utility driver generic interface                1.1
 *
 *  Generic interface for all drivers of the graphics utility used
 *  by the FreeType test programs.
 *
 *  Copyright 1996-1999 David Turner, Robert Wilhelm and Werner Lemberg.
 *
 *  This file is part of the FreeType project, and may only be used
 *  modified and distributed under the terms of the FreeType project
 *  license, LICENSE.TXT. By continuing to use, modify or distribute
 *  this file you indicate that you have read the license and
 *  understand and accept it fully.
 *
 ***************************************************************************)

unit GDriver;

{$mode objfpc}

interface


uses
  GEvents, GMain;

(* It is the responsability of the driver to map its events   *)
(* to the TEvent structure when called through Get_Event.     *)


type
  Event = record
    what: GEvent; (* event class     *)
    info: Int;    (* event parameter *)
  end;

(* The event classes are defined in the unit 'GEvents' included *)
(* by the test programs, not by the graphics utility            *)

procedure Get_Event(var ev: Event);
(* get last event. In full-screen modes, a key-stroke must be *)
(* translated to an event class with a parameter.             *)

function Driver_Set_Graphics(Mode: Int): Boolean;
(* A call to this function will set the graphics mode, the Vio    *)
(* variable, as well as the values vio_ScanLineWidth, vio_Width   *)
(* and vio_Height                                                 *)

function Driver_Restore_Mode: Boolean;
(* Restore previous mode or release display buffer/window *)

procedure Driver_Display_Bitmap(var Buffer; Lines, Columns: Int);
(* Display bitmap on screen *)


implementation


{$ifdef linux}
	{$i gdrv_sdl.inc}
{$endif}

{$ifdef kolibri}
	{$i gdrv_kos.inc}
{$endif}


type
  Translator = record
    key     : char;
    ev_class: GEvent;
    ev_info : Int;
  end;

const
  Num_Translators = 15;

  Translators : array[1..Num_Translators] of Translator
              = (
                  (key:#27; ev_class:event_Quit ;         ev_info:0),

                  (key:'x'; ev_class: event_Rotate_Glyph; ev_info:  -1),
                  (key:'c'; ev_class: event_Rotate_Glyph; ev_info:   1),
                  (key:'v'; ev_class: event_Rotate_Glyph; ev_info: -16),
                  (key:'b'; ev_class: event_Rotate_Glyph; ev_info:  16),

                  (key:'9'; ev_class: event_Change_Glyph; ev_info:-100),
                  (key:'0'; ev_class: event_Change_Glyph; ev_info: 100),
                  (key:'i'; ev_class: event_Change_Glyph; ev_info: -10),
                  (key:'o'; ev_class: event_Change_Glyph; ev_info:  10),
                  (key:'k'; ev_class: event_Change_Glyph; ev_info:  -1),
                  (key:'l'; ev_class: event_Change_Glyph; ev_info:   1),

                  (key:'+'; ev_class: event_Scale_Glyph; ev_info:  10),
                  (key:'-'; ev_class: event_Scale_Glyph; ev_info: -10),
                  (key:'u'; ev_class: event_Scale_Glyph; ev_info:   1),
                  (key:'j'; ev_class: event_Scale_Glyph; ev_info:  -1)
                );

procedure Process_Key(var ev: Event; c: Char);
var
  i: Int;
begin
  for i := 1 to Num_Translators do
  if c = translators[i].key then
  begin
    ev.what := translators[i].ev_class;
    ev.info := translators[i].ev_info;
    Exit;
  end;

  (* unrecognized keystroke *)
  ev.what := event_Keyboard;
  ev.info := Ord(c);
end;


procedure Get_Event(var ev: Event);
begin
  Process_Key(ev, ReadKey);
end;

end.
