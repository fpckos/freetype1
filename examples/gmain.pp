(***************************************************************************
 *
 *  GMain     Graphics utility main interface                            1.1
 *
 *  This unit defines a common interface.
 *  It relies on system dependent driver unit named 'GDriver'.
 *
 *  Copyright 1996-1999 David Turner, Robert Wilhelm and Werner Lemberg.
 *
 *  This file is part of the FreeType project, and may only be used
 *  modified and distributed under the terms of the FreeType project
 *  license, LICENSE.TXT. By continuing to use, modify or distribute
 *  this file you indicate that you have read the license and
 *  understand and accept it fully.
 *
 ***************************************************************************)

(***************************************************************************
 *
 *  This unit will been updated by bw for work under Free Pascal
 *  on Linux.
 *
 *  Copyright 2008 bw.
 *
 ***************************************************************************)

unit GMain;

{$mode objfpc}


interface


const
  Graphics_Mode_Mono = 1;
  Graphics_Mode_Gray = 2;


type
  {Int = -$80000000 .. 2147483647;}
  Int = Integer;


var
  vio_ScanLineWidth : Int;
  vio_Width         : Int;
  vio_Height        : Int;

  gray_palette : array[0..4] of Byte;  (* gray palette *)

  gcursor_x : Int;
  gcursor_y : Int;

  gwindow_width  : Int;
  gwindow_height : Int;

  Header: String;


function Set_Graph_Screen( mode : Int ) : boolean;
(* Set a Graphics Mode, chosen from the Graphics_Mode_xxx list *)

function Restore_Screen : boolean;
(* Restore a previous ( or text ) video mode *)

procedure Display_Bitmap_On_Screen( var buff; line, col : Int );
(* display bitmap of 'line' line, and 'col' columns ( each *)
(* column mode of 1 byte                                   *)

procedure Goto_XY(x, y: Int);
procedure Print_Str(str: String);
procedure Print_XY(x, y: Int; str: String);


implementation


uses
  GDriver;


type
  TByte = array[0..0] of Byte;
  PByte = ^TByte;

var
  Current_Mode : Byte;


procedure Print_8x8(x, y: Int; c: Char);
begin
  if (x=0) and (y=0) then
    Header := c else
  if c = #0 then (* do nothing *) else
  if Length(Header) < 255 then
    Header := Header + c;
end;


function Set_Graph_Screen(Mode: Int): Boolean;
begin
  Result := False;

  gcursor_x := 0;
  gcursor_y := 0;

  case Mode of
    Graphics_Mode_Mono: begin
      if not Driver_Set_Graphics(mode) then Exit;
      gwindow_width  := vio_ScanLineWidth;
      gwindow_height := vio_Height div 8;
    end;

    Graphics_Mode_Gray: begin
      if not Driver_Set_Graphics(mode) then Exit;
      gwindow_width  := vio_ScanLineWidth div 8;
      gwindow_height := vio_Height div 8;
    end;

    else
      Exit;
  end;

  Result := True;
end;


function Restore_Screen: Boolean;
begin
  gcursor_x := 0;
  gcursor_y := 0;

  gwindow_height := 0;
  gwindow_width  := 0;

  Restore_Screen := Driver_Restore_Mode;
end;


procedure Display_Bitmap_On_Screen( var buff; line, col : Int );
begin
  Driver_Display_Bitmap( buff, line, col );
end;


procedure Goto_XY(x, y: Int);
begin
  gcursor_x := x;
  gcursor_y := y;
end;


procedure Print_Str(str: String);
var
  i: Int;
begin
  for i := 1 to Length(str) do
  begin
    case str[i] of
      #13: begin
        gcursor_x := 0;
        Inc(gcursor_y);
        if gcursor_y > gwindow_height then
          gcursor_y := 0;
      end;

      else
        Print_8x8(gcursor_x, gcursor_y, str[i]);
        Inc(gcursor_x);
        if gcursor_x >= gwindow_width then
        begin
          gcursor_x := 0;
          inc( gcursor_y );
          if gcursor_y >= gwindow_height then gcursor_y := 0;
        end;
    end
  end
end;


procedure Print_XY(x, y: Int; str: String);
begin
  Goto_XY( x, y );
  Print_Str( str );
end;


end.
