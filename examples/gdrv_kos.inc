
const
  WIN_LEFT   = 50;
  WIN_TOP    = 100;
  WIN_WIDTH  = 640;
  WIN_HEIGHT = 450;


type
  TColor24 = packed record
    R, G, B: Byte;
  end;


var
  Screen: Pointer = nil;
  ScreenWidth : Longint = -1;
  ScreenHeight: Longint = -1;
  Palette: array[Byte] of TColor24;


procedure ShowCaption;
var
  Caption: AnsiString;
begin
  Caption := Header + #0;
  kos_setcaption(PChar(@Caption[1]));
end;


procedure KolibriPaint;
begin
  kos_begindraw();
  if (ScreenWidth > 0) and (ScreenHeight > 0) then
    kos_definewindow(WIN_LEFT, WIN_TOP, ScreenWidth - 1, ScreenHeight - 1, $64000000);

  if Assigned(Screen) then
     kos_drawimage24(0, 0, ScreenWidth, ScreenHeight, Screen);
  kos_enddraw();

  ShowCaption;
end;


function Driver_Set_Graphics(Mode: Int): Boolean;
var
  I: Integer;
  C: Byte;
  ThreadInfo  : TKosThreadInfo;
  WindowWidth : Longint;
  WindowHeight: Longint;

begin
  case Mode of
    Graphics_Mode_Gray: begin
      Vio_ScanLineWidth := WIN_WIDTH;
      Vio_Width         := WIN_WIDTH;
      Vio_Height        := WIN_HEIGHT;

      for I := 0 to 4 do
        gray_palette[I] := I;

      for I := 0 to 31 do
      begin
        C := Round((31 - I) / 31 * 255);
        Palette[I].R := C;
        Palette[I].G := C;
        Palette[I].B := C;
      end;

      for I := 32 to 255 do
      begin
        Palette[I].R := 255;
        Palette[I].G := 0;
        Palette[I].B := 255;
      end;

      Screen := GetMem(WIN_WIDTH * WIN_HEIGHT * SizeOf(TColor24));
      ScreenWidth  := WIN_WIDTH;
      ScreenHeight := WIN_HEIGHT;

      KolibriPaint;
      kos_threadinfo(@ThreadInfo);

      with ThreadInfo, WindowRect do
      begin
        WindowWidth  := Width  - ClientRect.Width  + Longint(ScreenWidth);
        WindowHeight := Height - ClientRect.Height + Longint(ScreenHeight);
        kos_movewindow(Left, Top, WindowWidth, WindowHeight);
      end;

      Result := True;
    end;

    else
      Result := False;
  end;
end;


function Driver_Restore_Mode: Boolean;
begin
  if Assigned(Screen) then
  begin
    FreeMem(Screen);
    Screen := nil;
  end;
  Result := True;
end;


procedure Driver_Display_Bitmap(var Buffer; Lines, Columns: Int);
var
  I, J : Integer;
  Color: TColor24;
  PSrc, PDst: PByte;
begin
  PSrc := @Buffer;
  PDst := Screen + (Lines - 1) * ScreenWidth * SizeOf(TColor24);

  for I := 0 to Lines - 1 do
  begin
    for J := 0 to Columns - 1 do
    begin
      Color := Palette[PSrc^];
      PDst^ := Color.R; Inc(PDst);
      PDst^ := Color.G; Inc(PDst);
      PDst^ := Color.B; Inc(PDst);
      Inc(PSrc);
    end;
    Dec(PDst, (Columns + ScreenWidth) * SizeOf(TColor24));
  end;

  KolibriPaint;
end;


function ReadKey: Char;
var
  Key: Word;

begin
  ShowCaption;
  kos_maskevents(ME_PAINT or ME_KEYBOARD or ME_BUTTON);

  while True do
  case kos_getevent() of
    SE_PAINT: KolibriPaint;

    SE_KEYBOARD: begin
      Key := kos_getkey() shr 8;
      if Key < 128 then
      begin
        Result := Char(Key);
        Break;
      end;
    end;

    SE_BUTTON: if kos_getbutton() = 1 then
    begin
      Result := #27;
      Break;
    end;
  end;
end;
