(***************************************************************************
 *
 *  GEvents   Test programs events definition                            1.1
 *
 *  This unit defines the events used by the FreeType test programs
 *  It is _not_ used by 'GMain'. This unit is also used by the drivers
 *  to translate their own events into GEvents.
 *
 *  Not a very good design, but we're not rewriting X..
 *
 *  Copyright 1996-1999 David Turner, Robert Wilhelm and Werner Lemberg.
 *
 *  This file is part of the FreeType project, and may only be used
 *  modified and distributed under the terms of the FreeType project
 *  license, LICENSE.TXT. By continuing to use, modify or distribute
 *  this file you indicate that you have read the license and
 *  understand and accept it fully.
 *
 ***************************************************************************)

Unit GEvents;

interface

type
  GEvent = (
             event_None,
             event_Quit,          (* Quit program *)

             event_Keyboard,      (* unknown keystroke *)

             event_Change_Glyph,
             event_Rotate_Glyph,
             event_Scale_Glyph,

             event_Change_ScanType,
             event_Change_Instructions
           );

implementation

end.
