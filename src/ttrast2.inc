(*******************************************************************
 *
 *  TTRast2.Inc                                                 1.2
 *
 *    Part of the FreeType glyph rasterizer (inline assembly).
 *    This version is used for the OS/2 Virtual Pascal compiler
 *
 *  Copyright 1996 David Turner, Robert Wilhelm and Werner Lemberg
 *
 *  This file is part of the FreeType project, and may only be used
 *  modified and distributed under the terms of the FreeType project
 *  license, LICENSE.TXT. By continuing to use, modify or distribute
 *  this file you indicate that you have read the license and
 *  understand and accept it fully.
 *
 ******************************************************************)

(****************************************************************************)
(*                                                                          *)
(* Function:    Split_Bezier                                                *)
(*                                                                          *)
(* Description: Subdivises one Bezier arc into two joint                    *)
(*              sub-arcs in the Bezier stack.                               *)
(*                                                                          *)
(* Input:       None ( subdivised bezier is taken from the top of the       *)
(*              stack )                                                     *)
(*                                                                          *)
(* Returns:     Nada                                                        *)
(*                                                                          *)
(****************************************************************************)

procedure Split_Bezier( base : PBezierStack );
var
  arc  : PBezierStack;
  a, b : Long;
begin
  asm
    push esi
    push ebx
    push ecx

    mov  esi, base

    mov  eax, [esi+2*8]       (* arc^[4].x := arc^[2].x *)
    mov  ebx, [esi+1*8]       (* b := arc^[1].x *)
    mov  ecx, [esi+0*8]       (* b := (arc^[0].x+b) div 2 *)

    mov  [esi+4*8], eax

    add  eax, ebx             (* a := (arc^[2].x+b) div 2 *)
    add  ebx, ecx
    mov  edx, eax
    mov  ecx, ebx
    sar  edx, 31
    sar  ecx, 31
    sub  eax, edx
    sub  ebx, ecx
    sar  eax, 1
    sar  ebx, 1

    mov  [esi+3*8], eax       (* arc^[3].x := a *)
    mov  [esi+1*8], ebx

    add  eax, ebx             (* arc[2].x := (a+b) div 2 *)
    mov  edx, eax
    sar  edx, 31
    sub  eax, edx
    sar  eax, 1
    mov  [esi+2*8], eax

    add  esi, 4

    mov  eax, [esi+2*8]       (* arc^[4].x := arc^[2].x *)
    mov  ebx, [esi+1*8]       (* b := arc^[1].x *)
    mov  ecx, [esi+0*8]       (* b := (arc^[0].x+b) div 2 *)

    mov  [esi+4*8], eax

    add  eax, ebx             (* a := (arc^[2].x+b) div 2 *)
    add  ebx, ecx
    mov  edx, eax
    mov  ecx, ebx
    sar  edx, 31
    sar  ecx, 31
    sub  eax, edx
    sub  ebx, ecx
    sar  eax, 1
    sar  ebx, 1

    mov  [esi+3*8], eax       (* arc^[3].x := a *)
    mov  [esi+1*8], ebx

    add  eax, ebx             (* arc[2].x := (a+b) div 2 *)
    mov  edx, eax
    sar  edx, 31
    sub  eax, edx
    sar  eax, 1
    mov  [esi+2*8], eax

    pop ecx
    pop ebx
    pop esi
  end;
end;



procedure Vertical_Sweep_Span( y     : Int;
                               x1,
                               x2    : TT_F26dot6;
                               Left,
                               Right : PProfile );
var
  e1, e2  : Longint;
  c1, c2  : Int;
  f1, f2  : Int;
  base    : PByte;
begin
  asm
    push esi
    push ebx
    push ecx

    mov  eax, X1
    mov  ebx, X2
    mov  ecx, [Precision_Bits]

    sub  ebx, eax
    add  eax, [Precision]
    dec  eax

    sub  ebx, [Precision]
    cmp  ebx, [Precision_Jitter]
    jg @No_Jitter

  @Do_Jitter:
    mov  ebx, eax
    jmp @0

  @No_Jitter:
    mov  ebx, X2

  @0:
    sar  ebx, cl
    js   @Sortie

    sar  eax, cl
    mov  ecx, [BWidth]

    cmp  eax, ebx
    jg   @Sortie

    cmp  eax, ecx
    jge  @Sortie

    test eax, eax
    jns  @1
    xor  eax, eax
  @1:
    cmp  ebx, ecx
    jl   @2
    lea  ebx, [ecx-1]
  @2:

    mov  edx, eax
    mov  ecx, ebx
    and  edx, 7
    sar  eax, 3
    and  ecx, 7
    sar  ebx, 3

    cmp  eax, [gray_min_X]
    jge  @3
    mov  [gray_min_X], eax

  @3:
    cmp  ebx, [gray_max_X]
    jl   @4
    mov  [gray_max_X], ebx

  @4:
    mov esi, ebx

    mov ebx, [BCible]
    add ebx, [TraceOfs]
    add ebx, eax

    sub esi, eax
    jz @5

    mov  al, [LMask + edx].byte
    or   [ebx], al
    inc  ebx
    dec  esi
    jz @6
    mov  eax, -1
  @7:
    mov [ebx].byte, al
    dec esi
    lea ebx, [ebx+1]
    jnz @7

  @6:
    mov al, [RMask + ecx].byte
    or [ebx], al
    jmp @8

  @5:
    mov al, [LMask + edx].byte
    and al, [RMask + ecx].byte
    or  [ebx], al

  @8:
  @Sortie:
    pop  ecx
    pop  ebx
    pop  esi
  end;
end;
