(*                                                                   *)
(* TTConfig.Inc                                                      *)
(*                                                                   *)
(* This file contains several definition pragmas that are used to    *)
(* build several versions of the library. Each constant is commented *)

(* Define the DEBUG constant if you want the library dumping trace *)
(* information to the standard error output.                       *)
{ $DEFINE DEBUG}


(* Define the ASSERT constant if you want to generate runtime integrity  *)
(* checks within the library. Most of the checks will panic and stop the *)
(* the program when failed..                                             *)
{ $DEFINE ASSERT}


(* Define this if you want to completely disable the use of the bytecode *)
(* interpreter.  Doing so will produce a much smaller library, but the   *)
(* quality of the rendered glyphs will enormously suffer from this.      *)
(*                                                                       *)
(* This switch was introduced due to the Apple patents issue which       *)
(* emerged recently on the FreeType lists.  We still do not have Apple's *)
(* opinion on the subject and will change this as soon as we have.       *)
{ $DEFINE TT_CONFIG_OPTION_NO_INTERPRETER}

(* Define the INLINE constant if you want to use inlining when provided  *)
(* by your compiler. Currently, only Virtual Pascal does                 *)
{$IFDEF VIRTUALPASCAL}
{$DEFINE INLINE}
{$ENDIF}

(* Define the USE32 constant on 32-bit systems. Virtual Pascal always    *)
(* define it by default. Now set for Delphi and FreePascal.              *)
{$IFDEF WIN32}
{$DEFINE USE32}
{$ENDIF}
{$IFDEF FPC}
{$DEFINE USE32}
{$ENDIF}

(* FreeType doesn't compile on old Pascal compilers that do not allow *)
(* inline assembly like Turbo Pascal 5.5 and below                    *)
{$IFDEF VER40}
ERROR : FreeType cannot be compiled with something older than Turbo Pascal 6.0
{$ENDIF}
{$IFDEF VER50}
ERROR : FreeType cannot be compiled with something older than Turbo Pascal 6.0
{$ENDIF}
{$IFDEF VER55}
ERROR : FreeType cannot be compiled with something older than Turbo Pascal 6.0
{$ENDIF}

(* Define the BORLANDPASCAL constant whenever you're using a DOS-based *)
(* version of Turbo or Borland Pascal.                                 *)
{$IFDEF VER60}
{$DEFINE BORLANDPASCAL}
{$ENDIF}
{$IFDEF VER10}    (* for Turbo Pascal 1.0 for Windows *)
{$DEFINE BORLANDPASCAL}
{$ENDIF}
{$IFDEF VER15}    (* for Turbo Pascal 1.5 for Windows *)
{$DEFINE BORLANDPASCAL}
{$ENDIF}
{$IFDEF VER60}
{$DEFINE BORLANDPASCAL}
{$ENDIF}
{$IFDEF VER70}
{$DEFINE BORLANDPASCAL}
{$ENDIF}

(* Define DELPHI16 when compiled in the 16-bit version of Delphi *)
{$IFDEF VER80}
{$DEFINE DELPHI16}
{$ENDIF}

(* Define DELPHI32 when compiled in any 32-bit version of Delphi *)
{$IFDEF VER90}     (* for Delphi 2 *)
{$DEFINE DELPHI32}
{$ENDIF}
{$IFDEF VER93}     (* for Borland C++ Builder 1 *)
{$DEFINE DELPHI32}
{$ENDIF}
{$IFDEF VER100}    (* for Delphi 3 *)
{$DEFINE DELPHI32}
{$ENDIF}
{$IFDEF VER110}    (* for Borland C++ Builder 3 *)
{$DEFINE DELPHI32}
{$ENDIF}
{$IFDEF VER120}    (* for Delphi 4 *)
{$DEFINE DELPHI32}
{$ENDIF}
{$IFDEF VER125}    (* for Borland C++ Builder 4 *)
{$DEFINE DELPHI32}
{$ENDIF}

(* I don't have Delphi 5, I hope this will work *)
{$IFDEF VER130}
{$DEFINE DELPHI32}
{$ENDIF}

(* I don't have Borland C++ Builder 5, I hope this will work *)
{$IFDEF VER135}
{$DEFINE DELPHI32}
{$ENDIF}

(* I don't know about Delphi 6, I hope this will work *)
{$IFDEF VER140}
{$DEFINE DELPHI32}
{$ENDIF}

(* I don't know about Borland C++ Builder 6, I hope this will work *)
{$IFDEF VER145}
{$DEFINE DELPHI32}
{$ENDIF}
